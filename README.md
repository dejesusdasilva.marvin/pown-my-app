# Pown my app

XSS, SQLI, CSRF

Les vulnerabilités XSS, SQLI, CSRF sont très connus, mais comment s'en proteger?

Pour ce projet j'ai crée certaines applications avec les technos suivantes:

    1.Langage php,
    2.XAMPP qui me permet de mettre en place un serveur Web local
    3.Connection mysql avec PhpAdmin

## Comment lancer le fichier php?

Tout d'abord installez XAMPP: <https://www.apachefriends.org/fr/index.html> ,

Ensuite placez le projet dans xampp.htdocs.

Lancez Apache et Mysql et voilà, il faut juste placer l'url sur un navigateur avec localhost[nom du repertoire où se trouve le fichier], et commencer à explorer les failles de sécurité.

Voyons ensemble comment ça fonctionne et quelles sont les moyens de s'y proteger.

## xss

Problème :
    Il est possible d'injecter des scripts js dans les variables posté utilisé avec HTTPPOST.

Solution:
    Empêcher les tags html et les convertir en entités html, permettant d'être fidèle à ce qui est tapé dans le "textearea"

    par exemple: 

    $_POST['com']=htmlentities($_POST['com']);
     
     ou

    Enlever tous les tags html posté dans un formulaire avec la fonction strip_tags;
    
    exemple:  
    
    $_POST['com']=strip_tags($_POST['com']);

## SQLI

    Problème :

    Vulnerable à l'injection SQL 'OR 1=1 OR 1=' , la majorité des serveurs possede magic quotes, si on commente les variables posté, on vera que le serveur ajoutera automatiquement les slahshes dans la commande sql, mais ce n'est pas le cas presenté ici, donc on va créer une fonction qui va ajouter automatiquement les slashes,et par consequent empecher l'éxecution de la commande.

    comme ceci: $login = addslashes($login, $login);

## CSRF
